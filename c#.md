# Object Oriented Programming


Inheritance:
---

_What is inheritance?_

It allows us to use a class as a parent (or a base) class and pass on Fields, Properties, Methods and Constructors to other classes.

_Note: To be able to use inheritance we need to create a minimum of 2 classes_


Let’s use the class Person as our base class
```c
public class Person
{
    protected string Name {get; set;} //can be public, but not private
    protected int Age {get; set;}

    public Person(string name, int age) {
        Name = name;
        Age = age;
    }

    public void SayHello()
    {
	    Console.WriteLine($"Hello World!");
	    Console.WriteLine($"My name is {Name} and I am {age}!");
    }
}

public class PoliceOfficer : Person
{
    public int PoliceId {get; set;} 

    public PoliceOfficer(int policeid, string Name, int Age) : base (Name, Age) {

        PoliceId = policeid

    }

    public void Arrest()
    {
	    Console.WriteLine($"Logic for arresting you");
    }
}
```
You may have noticed the accessor keyword protected in the base class. Anything with a protected accessor can be accessed by the class itself or the direct child of that class.

Using Inheritance
---

```c
public static void Main(string[] args)
{
    var p1 = new PoliceOfficer(3456, "John", 45);

	p1.SayHello();
	p1.Arrest();
}
```
* Inheritance and Encapsulation go hand in hand in all cases of OOP

* Now we can create classes that are dependent on other classes

* Because of this Inheritance should only be used inheritance makes sense (Do not be lazy)

* You can only inherit what you are given from the parent (base) class

