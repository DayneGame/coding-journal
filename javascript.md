# Variables 

A variable is a name for a value, so that we can reference to a word rather than a value

``` js
var name = "Dayne"
```
---

# JS - console.log with string interpolation

``` js
var name = "Dayne"

console.log("Hi my name is name") //output: Hi my name is name

console.log(`Hi my name is ${name}`) //output: Hi my name is Dayne
```
---
# User Input
```js
var person = prompt("Please enter your name", "Dayne Game");
```
